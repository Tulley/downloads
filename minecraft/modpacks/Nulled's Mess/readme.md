# Minimal System Requirements
6GB DDR3 RAM
GTX 965 or similar

### How to increase RAM:
- Launch Vanilla Minecraft Launcher
- Click on Installations tab
- Click on ... for 1.12.2-forge, then click Edit
- In JVM Arguments, modify -Xmx to at least "-Xmx6GB" or more if possible


# First Time Install

- Launch Minecraft Launcher, download and start 1.12.2
- Once 1.12.2 has loaded, close 1.12.2
- Download Forge from the provided URL below
https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.12.2-14.23.5.2838/forge-1.12.2-14.23.5.2838-installer-win.exe
- In the Installer, select Client and press Ok
- Hit Windows Key + R to open Run, and paste "%appdata%/.minecraft", hit enter
- Copy mods and mods-resourcepacks directly into .minecraft folder
- Copy 1.12.2-forge1.12.2-14.23.5.2838 into versions
- Restart Minecraft Launcher, 1.12.2-forge1.12.2-14.23.5.2838 should be an playable option now
- Play 1.12.2-forge

# Updating Modpack

- Hit Windows Key + R to open Run, and paste "%appdata%/.minecraft", hit enter
- Copy mods and mods-resourcepacks directly into .minecraft folder
- Restart Minecraft Launcher, 1.12.2-forge1.12.2-14.23.5.2838 should be an playable option now
- Play 1.12.2-forge